//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3020;

// Se agrega excepiciones de seguridad para poder hacer metodos post desde navegador
// Se configura el acceso seguro para MLab
var bodyParser = require ('body-parser');
app.use(bodyParser.json());
// A todas las peticiones se le agrega el header
app.use(function(req,res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
 });

// Se agrega la dependencie a libreria request-json (nos ayudara a generar request a aPIS con respuesta JSON)
var requestjson = require ( 'request-json' );

var path = require('path');

// Variable contador de ID
var idCounter = 0;

// Se adiciona la url de la API donde esta almacenada nuestro MongoDB
var urlMovimientos = "https://api.mlab.com/api/1/databases/bdbanca3mb79567/collections/movimientos?apiKey=DjMnkFPl3anX6efNEI9FdIEmIZtRZ0jl";

// Se genera el cliente con la API request-json y la URL de la API a consumir
var clienteMLab = requestjson.createClient(urlMovimientos);


app.listen(port);

console.log('todo list RESTful API server started on FDL: ' + port);

//----------------------------------------------------------------------------------
// Añadimos las consultas GET NODE para la API Mongo DB (GET)
app.get("/movimientos", function(req, res)
{
  // Se genera el cliente con la API request-json y la URL de la API a consumir
  //var clienteMLab = requestjson.createClient(urlMovimientos);

  // Se hace el GET en el cliente.
  clienteMLab.get('', function(err, resM, body){
    if ( err)
    {console.log(err);
      alert(err);
    }else{
      //Envia la peticion al servidor
      res.send(body);
    }
  });

} );


//----------------------------------------------------------------------------------
// Se define la peticion app POST  para insertar en NODE
app.post("/movimientos", function(req,res)
{
    //
    clienteMLab.post ('', req.body, function(err, resM, body){
      if(err)
      {
        console.log(err);
      }else {
        res.send(body);
      }
    }
  );

});


//----------------------------------------------------------------------------------
// Se define la peticion app get y redirige a directorio raiz /index.html
app.get('/', function(req, res)
{
  //res.send("Hola Mundo node");
  res.sendfile(path.join(__dirname, "index.html"));
}
);

//Definiendo la estructura del API RESTful /clientes  y nos trae un JSON con clientes
app.get('/clientes/', function(req, res)
{
  res.sendfile(path.join(__dirname, "index.html"));
  var clientes =
    [  { idCliente: 1,    nombre: "Fidel",    apellido: "Villalobos" },
       { idCliente: 2,    nombre: "Antonio",  apellido: "Jaimes" },
       { idCliente: 3,    nombre: "Ricardo",  apellido: "Salas" },
       { idCliente: 4,    nombre: "Alejandro",apellido: "Contreras" }];

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(clientes));
} );

//Definiendo la estructura del API RESTful Clientes pero se buscan con un ID
app.get('/clientes/:idCliente', function(req, res)
{
  res.send("GET: Aqui tiene el cliente numero: " + req.params.idCliente);
//  res.setHeader('Content-Type', 'application/json');
//  res.send(JSON.stringify(clientes));

}
);

app.post('/', function(req, res)
{
  res.send("Su petición POST ha sido recibida y actualizada");
}
);

app.put('/', function(req, res)
{
  res.send("Su petición PUT ha sido recibida");
}
);


app.delete('/', function(req, res)
{
  res.send("Su petición DELETE ha sido recibida");
}
);
